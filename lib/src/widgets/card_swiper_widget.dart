import 'package:flutter/material.dart';

import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:peliculas/src/models/pelicula_model.dart';

class CardSwiperWidget extends StatelessWidget {
  final List<Pelicula> peliculas;

  CardSwiperWidget({@required this.peliculas}); // Constructor

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;

    return Container(
      padding: EdgeInsets.only(top: 25.0),
      child: Swiper(
        layout: SwiperLayout.STACK,
        itemWidth:
            _screenSize.width * 0.6, // para obtener el 70% de la pantalla
        itemHeight:
            _screenSize.height * 0.5, // para obtener el 50% de la pantalla
        itemBuilder: (BuildContext context, int position) {
          peliculas[position].uniqueId = '${peliculas[position].id}-card';
          return _crearTarjetaSwipe(context, position);
        },
        itemCount: peliculas.length,
        //pagination: new SwiperPagination(), // Puntos de pagination
        //control: new SwiperControl(), // Flechas de swiper
      ),
    );
  }

  Widget _crearTarjetaSwipe(BuildContext context, int position) {
    final image = GestureDetector(
      child: FadeInImage(
        placeholder: AssetImage('assets/img/no-image.jpg'),
        image: NetworkImage(peliculas[position].getPosterImg()),
        fit: BoxFit.cover,
      ),
      onTap: () {
        Navigator.pushNamed(context, 'detalle', arguments: peliculas[position]);
      },
    );

    return Hero(
      tag: peliculas[position].uniqueId,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20.0),
        child: GestureDetector(
          child: image,
        ),
      ),
    );
  }
}
