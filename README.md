# peliculas

## Getting Started

# Parte1
Temas puntuales de la sección
Estos son algunos de los temas principales de la sección:

Consumo de servicios REST

Conversión de JSON --> Maps --> Modelos

PageViews

ApiKeys

Providers

Streams

Infinite Horizontal Scroll

SearchDelegate

Esta es una aplicación real que usaremos eventualmente para desplegarla en las
AppStores, pero principalmente porque aprenderemos a crear una aplicación real,
útil y funcional. Sin contar que aprenderemos muchas cosas en el camino

# Parte2

Temas puntuales de la sección
Temas de la sección:

Google PlayStore

Apple AppStore

Generación del APK de Android de 32 y 64bits

Cambiar ícono de la aplicación

SplashScreen

Carga a la PlayStore

Carga a la AppStore

Configuraciones de AppStore Connect

Configuraciones en el portal de developer de Apple

Esta sección tiene por objetivo ayudarlos con los despliegues de sus
aplicaciones en las tiendas, junto a una guía paso a paso detallada para
un despliegue exitoso.